#基本使用流程：
`失败任务重发模块扩展 —— 支持 “非加密” / “Des加密” 两种模式`
### 1. composer下载扩展
    > composer require iflycc/notify-retry
### 2. 执行数据库迁移，mysql连接配置
    配置连接：src/config/db.php --配置数据库连接接  
    执行迁移：
        > cd src/cmd;
        > php migration.php; --数据表创建
### 3. 加载模块启动脚本
     * 如果你的项目是严格执行了PSR-4标准，则可以直接调用当前扩展的类和方法。
     * 如果是老项目，则需要在项目入口引入本模块的自动加载文件：
        > include __DIR__ . '/vendor/autoload.php';  --引入模块加载器
### 4. 配置通知远程服务器的相关参数
    * src/config/notifications.php  --各个游戏服务器的通知参数配置
    * 如果需要以Des加密/解密的方式发送-接收http请求，只需要在该文件中配置 "is_des: 1"即可。
### 5. 发送远端任务通知
    示例：
        $appId = 100;
        $channelId = 9999999;
        $versionCode = 31;
        $uid = '123456';
        $httpRequestParam = [ "value" => "1_1_100", "name" => "张三" ];
        $notifyConf = Func::config("notifications.{$appId}.cd_key"); //例：需要通知“兑换码”
        $notifyEntity = NotifyConfigEntity::toEntity($notifyConf); //转换成通知实体对象
        //执行通知 
        Tasks:notifyingGame($appId, $channelId, $versionCode,$uid, $httpRequestParam, $notifyEntity);  
### 6. 通知失败时，执行重新通知任务
    当Tasks:notifyingGame()执行失败后：
        会在 "retry_notify.retry_tasks"表中生成一条记录，用于下次重发的依据。

    命令行执行：（一般是配置Linux定时器自动执行）
        > cd src/cmd;
        > php run.php;  --执行失败任务重新通知，执行后通知成功的任务会删除任务记录，并且不论成功or失败，都会生成一条远程服务器的响应记录。