<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 15:38
 *
 * tip - test script!
 */

use Iflycc\Notify\controller\TestController;
use Iflycc\Notify\service\Params;

include __DIR__ . '/vendor/autoload.php';



$obj = new TestController();
$params = Params::instance();
$params->desChange(false);
//$obj->run($params);
$obj->test();