<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc6849df300db722c30401b087dd5f23d
{
    public static $prefixLengthsPsr4 = array (
        'I' => 
        array (
            'Iflycc\\Notify\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Iflycc\\Notify\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc6849df300db722c30401b087dd5f23d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc6849df300db722c30401b087dd5f23d::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitc6849df300db722c30401b087dd5f23d::$classMap;

        }, null, ClassLoader::class);
    }
}
