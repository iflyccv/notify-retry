<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '8f9dd6446381db85d38ba970a9294d1af4c4461a',
        'name' => 'iflycc/notify-retry',
        'dev' => true,
    ),
    'versions' => array(
        'iflycc/notify-retry' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '8f9dd6446381db85d38ba970a9294d1af4c4461a',
            'dev_requirement' => false,
        ),
    ),
);
