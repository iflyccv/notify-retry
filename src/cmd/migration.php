<?php
// +----------------------------------------------------------------------
// | Author: iflycc
// +----------------------------------------------------------------------
// | Tip： 数据库初始化
// +----------------------------------------------------------------------
// | 执行示例：
// |    1. 进入到配置的php安装路径，如：windows环境的php.exe所在位置；linux环境的php执行路径
// |    2. 执行： php migration.php
// +----------------------------------------------------------------------
// | 本地执行： /g/cc_softwares/phpStudy-V8.1.13/phpStudy_64/install/phpstudy_pro/Extensions/php/php5.5.9nts/php migration.php
// +----------------------------------------------------------------------
use Iflycc\Notify\service\Func;

include __DIR__ . '/../../vendor/autoload.php';

//① 连接数据
$con = mysqli_connect(Func::config('db.host'), Func::config('db.username'),Func::config('db.password'));
if(!$con){
    die("mysql 连接失败： 请确认数据库配置！" );
}
mysqli_set_charset($con, 'utf-8');
//② 读取sql文件内容并执行sql
$sql = file_get_contents(__DIR__ . '/migration.sql');
//$con->query($sql);
$sqlItems = explode(';', $sql);
foreach ($sqlItems as $_sql){
    if(!trim($_sql)) continue; //过滤空值
    $con->query($_sql);
}
//③ 关闭连接
$con->close();
//④ 打印成功信息
echo "Congratulations! Migrate mysql tables success!!!", PHP_EOL;

