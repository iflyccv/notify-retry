<?php
// +----------------------------------------------------------------------
// | Author: iflycc
// +----------------------------------------------------------------------
// | Tip： 命令行执行 `失败任务重发`机制
// +----------------------------------------------------------------------
// | 执行示例：
// |    1. 进入到配置的php安装路径，如：windows环境的php.exe所在位置；linux环境的php执行路径
// |    2. 执行： php run.php
// +----------------------------------------------------------------------
// | 本地执行： /g/cc_softwares/phpStudy-V8.1.13/phpStudy_64/install/phpstudy_pro/Extensions/php/php5.5.9nts/php run.php
// +----------------------------------------------------------------------
use Iflycc\Notify\dao\entity\RetryTasksEntity;
use Iflycc\Notify\dao\RetryTasksModel;
use Iflycc\Notify\service\Tasks;

include __DIR__ . '/../../vendor/autoload.php';

//开始执行
(new TasksCmd())->run();

/**
 *
 * Class TasksCmd
 */
class TasksCmd{
    private $NOW_TS; //执行时的时间戳

    /**
     * TasksCmd constructor.
     */
    public function __construct(){
        $this->NOW_TS = time();
    }

    /**
     * 执行入口
     */
    public function run(){
        $this->NOW_TS = time(); //精确到：分钟
        $mts1 = microtime(true) * 1000;
        //① 读取全部的`待执行任务列表`
        $tasksEntities = RetryTasksModel::select()->toEntity() ?: [];
        //② 依次执行
        $executeLog = [
            'start_time       ' => date('Y-m-d H:i:s'), //开始时间
            'end_time         ' => '', //结束时间
            'spend_millisecond' => 0,  //耗时（毫秒）
            'sum              ' => 0, //总任务数
            'success          ' => 0, //执行成功的数量
            'filter           ' => 0, //过滤掉的
            'fail             ' => 0, //执行失败的数量
        ];
        foreach ($tasksEntities as $_taskEntity){
            //根据上次执行时间、已执行次数，判断是否可以执行
            $_lastSendDt = (string)$_taskEntity->lastSendTime; //格式化时间格式
            $_doneTimes = (int)$_taskEntity->doneTimes; //已执行次数
            $executeLog['sum              '] += 1; //总数+1
            if(!self::_enableExecuteChecking($_lastSendDt, $_doneTimes)) {
                $executeLog['filter           '] += 1; //过滤掉的 +1
                continue;
            }
//            print_r($_taskEntity);exit;
            //通过检测，循环执行每个任务
            $_isSuccess = self::_taskRunning($_taskEntity);
            if($_isSuccess){
                $executeLog['success          '] += 1; //成功 +1
            }else{
                $executeLog['fail             '] += 1; //失败 +1
            }
        }
        //③ 记录执行日志打印
        $mts2 = microtime(true) * 1000;
        $executeLog['end_time         '] = date('Y-m-d H:i:s');
        $executeLog['spend_millisecond'] = round($mts2 - $mts1, 2) . ' ms';
        echo PHP_EOL,'▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁ 执行情况统计： ▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁',PHP_EOL;
        $statisticStr = '';
        foreach ($executeLog as $_rmk => $_v){
            $statisticStr .= "   ▶ {$_rmk} : \t{$_v}" . PHP_EOL;
        }
        echo $statisticStr;
    }

    /**
     * 每个任务的执行体
     * @param RetryTasksEntity $taskEntity
     * @return bool
     */
    private function _taskRunning(RetryTasksEntity $taskEntity){
        //① 发出通知
        $httpRequestParam = json_decode(urldecode($taskEntity->httpRequestParam) , true) ?: []; //db字段转为array
        $isTaskSuccess = Tasks::sending($taskEntity->appId, $taskEntity->channelId, $taskEntity->versionCode,
            $taskEntity->uid, $taskEntity->sessionId, $taskEntity->url, $taskEntity->httpRequestMethod, $taskEntity->isDes, $httpRequestParam);
        //② 根据重发结果，处理`任务`表
        if($isTaskSuccess){ //通知成功了，则从`重试任务表`中移除当前任务记录
            RetryTasksModel::where(['id' => $taskEntity->id])->delete(); //删除当前`任务`
        }else{ //失败了，则判断当前`任务`的的可执行次数，如果小于等于0则删除当前任务；否则更新`任务表`，可执行次数减一
            if($taskEntity->retryTimes - 1 <= 0){ //可执行次数不足，删除当前任务
                RetryTasksModel::where(['id' => $taskEntity->id])->delete();
            }else{
                $taskEntity->retryTimes -= 1; //可重发次数 -1
                $taskEntity->doneTimes += 1;  //已重发次数 -1
                $taskEntity->lastSendTime = date('Y-m-d H:i:s');  //更新最后一次发送时间
                RetryTasksModel::update($taskEntity);
            }
        }

        echo
        'task id : ',$taskEntity->id,' | ',
        'done times : ',$taskEntity->doneTimes,' | ',
        'last Done dt : ', $taskEntity->lastSendTime, ' | ' ,
        'is task success : ', $isTaskSuccess ? 1 : 0,
        PHP_EOL;

        //③ 返回处理结果
        return $isTaskSuccess;
    }

    /**
     * 根据执行的次数，控制本次是否执行任务重发
     *      - 已执行 0 次：马上执行
     *      - 已执行 1 次：3分钟后执行
     *      - 已执行 2 次：10分钟后执行
     *      - 已执行 3 次：30分钟后执行
     *      - 已执行 n 次：每60分钟执行一次，直到耗尽可重试次数为止
     *
     * @param string $lastDoneDt 上次执行的时间戳
     * @param int $doneTimes 已经执行的次数
     * @return bool
     */
    private function _enableExecuteChecking($lastDoneDt, $doneTimes){
        $result = false;
        $lastDoneTs = strtotime($lastDoneDt); //上次执行时间戳
        switch ($doneTimes){
            case 0:
                $result = true;
                break;
            case 1:
                if($this->NOW_TS - $lastDoneTs >= 3 * 60) $result = true;
                break;
            case 2:
                if($this->NOW_TS - $lastDoneTs >= 10 * 60) $result = true;
                break;
            case 3:
                if($this->NOW_TS - $lastDoneTs >= 30 * 60) $result = true;
                break;
            default: //前4次都执行失败了，则之后为：每2h/执行一次
                if($this->NOW_TS - $lastDoneTs >= 60 * 60) $result = true;
        }
        return $result;
    }
}