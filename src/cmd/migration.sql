create database retry_notify;
use retry_notify;

-- 创建“重发任务表”
drop table if exists `retry_tasks`;
CREATE TABLE `retry_tasks` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `session_id` varchar(64) NOT NULL DEFAULT '' COMMENT '会话session_id',
    `app_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '游戏id',
    `channel_id` varchar(32) NOT NULL DEFAULT '' COMMENT '渠道id',
    `uid` varchar(32) NOT NULL DEFAULT '',
    `version_code` int(11) NOT NULL COMMENT '版本号',
    `url` varchar(255) DEFAULT '' COMMENT '待发送的url链接',
    `http_request_method` varchar(32) NOT NULL DEFAULT '' COMMENT 'http请求方式',
    `is_des` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否des加密，0 / 1，如果=1，则需对response做des解密',
    `http_request_param` text NOT NULL COMMENT 'http请求参数',
    `retry_times` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '重送的次数',
    `done_times` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '已执行的次数',
    `last_send_time` datetime DEFAULT NULL COMMENT '最后一次发送的时间',
    `created_at` datetime DEFAULT NULL COMMENT '创建于',
    PRIMARY KEY (`id`),
    KEY `sessionIndex` (`session_id`) USING BTREE COMMENT 'session会话索引',
    KEY `urlIndex` (`url`) USING BTREE COMMENT 'url索引'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='通知重发机制 - 重发任务表';

-- 创建“重发响应表”
CREATE TABLE `retry_response` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `session_id` varchar(64) NOT NULL DEFAULT '' COMMENT 'session会话id',
    `app_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '游戏id',
    `channel_id` varchar(32) NOT NULL DEFAULT '' COMMENT '渠道id',
    `uid` varchar(32) NOT NULL DEFAULT '',
    `version_code` int(11) NOT NULL COMMENT '版本号',
    `url` varchar(128) NOT NULL COMMENT '请求链接',
    `http_request_method` varchar(32) NOT NULL DEFAULT '' COMMENT 'http请求方式',
    `is_des` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否des加密，0 / 1，如果=1，则需对response做des解密',
    `is_success` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否成功',
    `http_request_param` text NOT NULL COMMENT 'http请求参数',
    `http_response_origin` text NOT NULL COMMENT 'http对端响应（原生响应）',
    `http_response_after_des` text NOT NULL COMMENT 'http对端响应（Des解密后）',
    `remark` text NOT NULL COMMENT '备注',
    `created_at` datetime DEFAULT NULL COMMENT '创建于',
    PRIMARY KEY (`id`),
    KEY `sessionIndex` (`session_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='通知重发机制 - 重发响应表';