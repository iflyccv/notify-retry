<?php
/**
 * Author: iflycc
 * Time: 2023.07.29 21:07
 *
 * tip - `重发任务表`模型
 */
namespace Iflycc\Notify\dao;

use Exception;
use Iflycc\Notify\dao\entity\RetryTasksEntity;
use Iflycc\Notify\dao\impl\IRetryTasks;
use Iflycc\Notify\dao\impl\mongo\RetryTasksMongoImpl;
use Iflycc\Notify\dao\impl\mysql\RetryTasksMysqlImpl;

/**
 * Class RetryResponseModel
 * @package Iflycc\Notify\dao
 * @mixin IRetryTasks
 *
 * @method $this where(string|array $where) static 操作条件
 * @method $this find() 查一条
 * @method $this select() 查多条
 * @method int add(RetryTasksEntity $entity) 新增，返回last id
 * @method int update(RetryTasksEntity $entity) 更新，返回受影响的行数
 * @method string getLastSql() 返回最后一次执行的sql语句
 * @method RetryTasksEntity|RetryTasksEntity[] toEntity() 转换为实例对象
 * @method void delete() 删除
 */
class RetryTasksModel extends Model {
    /**
     * 获取配置的驱动db实例
     * @return IRetryTasks
     */
    public function db(){
        $driverImpl = null;
        try {
            switch ($this->driver) {
                case 'mysql':
                    $driverImpl = RetryTasksMysqlImpl::instance();
                    break;
                case 'mongo':
                    $driverImpl = RetryTasksMongoImpl::instance();
                    break;
                case 'other db-driver': //TODO | 其他数据库驱动，如：mongodb（需自定义实现）
                    break;
            }
            if(is_null($driverImpl)){
                throw new Exception("未实现的数据驱动: {$this->driver}");
            }
        } catch (Exception $e) {
            echo $e->getMessage() . " | " . $e->getTraceAsString();
            exit;
        }
        return $driverImpl;
    }
}