<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 19:58
 *
 * tip - `重发任务表`的mysql实现
 */
namespace Iflycc\Notify\dao\impl\mongo;
use Exception;
use Iflycc\Notify\dao\entity\RetryTasksEntity;
use Iflycc\Notify\dao\impl\IRetryTasks;
use Iflycc\Notify\service\Func;
use MongoDB\BSON\ObjectId;

class RetryTasksMongoImpl implements IRetryTasks {
    use CommonMongoImpl; //实现的部分公共的方法

    const DB_NAMESPACE = 'retry_notify.retry_tasks'; //申明表名 TODO | 根据自身环境可自定义配置
    const ENTITY_CLASS = RetryTasksEntity::class; //申明实体类
    private $driver; //驱动
    private static $instance; //自身静态实例
    private $where = []; //where条件
    private $data = []; //保存查询出来的数据
    private $entityVarsConvertFields = []; //实体类的自定义的字段（必须与数据库保持一致，不可随意改动）
    private $toEtyContainer = []; //实体对象转换是的中转容器

    /**
     * @return RetryTasksMongoImpl
     */
    public static function instance(){
        if(!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 新增
     * @param RetryTasksEntity $entity
     * @param string $replaceField 替换写入的字段
     * @return int 返回自增长id
     */
    public function add(RetryTasksEntity $entity,  $replaceField = ''){
        //过滤掉对象中的null
        $row = $this->_filterNull($entity->toArray());
        return $this->driver->insert($row, $replaceField);
    }

    /**
     * 更新
     * @param RetryTasksEntity $entity
     * @return int 返回首影响的行数
     */
    public function update(RetryTasksEntity $entity){
        //过滤掉对象中的null
        $row = $this->_filterNull($entity->toArray());
        if($entity->id){ //主键id存在，忽略其他条件，更新主键id更新
            unset($row['id']);
            $affectedCounts = $this->driver->where(['_id' => new ObjectId($entity->id)])->update($row);
        }else{
            $affectedCounts = $this->driver->where($this->where)->update($row);
        }
        return $affectedCounts;
    }

    /**
     * this.data转为entity对象实例
     *      - 只支持一维、二维数组数据的转换，否则就抛出异常
     * @return RetryTasksEntity|RetryTasksEntity[]
     */
    public function toEntity(){
        $result = null;
        try {
            if(!$this->toEtyContainer) $this->toEtyContainer = $this->data;
            //将`_id`字段转为`id`
            if(isset($this->toEtyContainer['_id'])){
                $temp = $this->toEtyContainer['_id'];
                unset($this->toEtyContainer['_id']);
                $this->toEtyContainer['id'] = is_array($temp) ? current($temp) : $temp;
            }

            foreach ($this->toEtyContainer as $_key => $_item){
                //是数字，则表示是“二维数组”，格式：[{item1}, {item2} ...]
                if(is_numeric($_key)){
                    $this->toEtyContainer = $_item;
                    $result[] = $this->toEntity(); //数字表示是多维数组，则递归调用自身
                    //数据转为正常，继续向下执行
                }else{ //一维数组
//                    print_r($this->entityVarsConvertFields);
//                    print_r(array_keys($this->toEtyContainer));
                    if(array_diff($this->entityVarsConvertFields, array_keys($this->toEtyContainer))){
                        throw new Exception('非法的数据，mongo数据转换为对象失败，请确认数据源与实体对象的字段对应关系！');
                    }
                    //通过检测，则退出循环
                    $entityObj = new RetryTasksEntity(); //实例化实体类
                    foreach ($this->entityVarsConvertFields as $_field){
                        $_var = Func::convert_ToUcWord($_field);
                        $entityObj->$_var = $this->toEtyContainer[$_field];
                    }
                    $result = $entityObj;
                    break;
                }
            }
        }catch (Exception $e){
            $err = [
                'err_file' => $e->getFile(),
                'err_line' => $e->getLine(),
                'err_previous' => $e->getPrevious(),
                'err_message' => $e->getMessage(),
                'err_track' => $e->getTraceAsString(),
            ];
            echo json_encode($err, JSON_UNESCAPED_UNICODE); exit;
        }
        return $result;
    }
}