<?php
/**
 * Author: iflycc
 * Time: 2023-8-8 11:11:18
 *
 * tip - 任务系统的公共实现方法
 */
namespace Iflycc\Notify\dao\impl\mongo;
use Iflycc\Notify\dao\drivers\mongo\BasicMongo;
use Iflycc\Notify\service\Func;
use MongoDB\BSON\ObjectId;

trait CommonMongoImpl{

    /**
     * 连接Mongo
     */
    public function __construct(){
        $host = Func::config('db.host');
        $port = Func::config('db.port');
        $this->driver = new BasicMongo($host, $port);
        $this->driver->namespaces(self::DB_NAMESPACE);
        //根据`实体类`初始化数据库表字段
        foreach (get_class_vars(self::ENTITY_CLASS) as $_field => $_val){
            $this->entityVarsConvertFields[] = Func::convertUcWordTo_($_field);
        }
    }

    /**
     * @param array $where sql条件
     * @return $this
     */
    public function where($where){
        $this->where = $where;
        //`id`转为`_id`
        if(isset($this->where['id'])){
            $this->where['_id'] = new ObjectId($this->where['id']);
            unset($this->where['id']);
        }
        return $this;
    }

    /**
     * 查单条
     * @return $this
     */
    public function find(){
        $data = $this->driver->where($this->where)->findOne() ?: [];
        $this->data = $data;
        return $this;
    }

    /**
     * 查多条
     */
    public function select(){
        $data = $this->driver->where($this->where)->find();
        $this->data = $data;
        return $this;
    }

    /**
     * 删除
     */
    public function delete(){
        $this->driver->where($this->where)->delete();
    }

    /**
     * 获取最近一直的sql
     * @return string
     */
    public function getLastSql(){
        return '';
    }

    /**
     * 过滤掉数组中的null值
     * @param array $row
     * @return array
     */
    private function _filterNull(array $row){
        //过滤掉对象中的null
        return array_filter($row, function ($item){
            return !is_null($item); //当内置函数返回false时，过滤
        });
    }
}