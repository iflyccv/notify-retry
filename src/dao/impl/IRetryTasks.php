<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 17:44
 *
 * tip - `重发任务表`接口
 */
namespace Iflycc\Notify\dao\impl;

use Iflycc\Notify\dao\entity\RetryTasksEntity;

interface IRetryTasks{
    public function where($where); //传递where条件

    public function find(); //查单条

    public function select(); //查多条

    public function add(RetryTasksEntity $entity); //入库

    public function update(RetryTasksEntity $entity); //更新

    public function delete(); //更新

    public function toEntity(); //转为实体类

    public function getLastSql(); //获取最近一次执行的sql语句

}