<?php
/**
 * Author: iflycc
 * Time: 2023.07.28 11:41
 *
 * tip - 任务系统的公共实现方法
 */
namespace Iflycc\Notify\dao\impl\mysql;
use Iflycc\Notify\dao\drivers\mysql\BasicMySql;
use Iflycc\Notify\service\Func;

trait CommonMysqlImpl{

    /**
     * 连接PDO
     */
    public function __construct(){
        $host = Func::config('db.host');
        $port = Func::config('db.port');
        $username = Func::config('db.username');
        $password = Func::config('db.password');
        $dsn = "mysql:host={$host};port={$port};charset=utf8;";
        $this->driver = new BasicMySql($dsn, $username, $password);
        $this->driver->table(self::TABLE);
        //根据`实体类`初始化数据库表字段
        foreach (get_class_vars(self::ENTITY_CLASS) as $_field => $_val){
            $this->entityVarsConvertFields[] = Func::convertUcWordTo_($_field);
        }
    }

    /**
     * @param array|string $where sql条件
     * @return $this
     */
    public function where($where){
        $this->where = $where;
        return $this;
    }

    /**
     * 查单条
     * @return $this
     */
    public function find(){
        $data = $this->driver->where($this->where)->find() ?: [];
        $this->data = $data;
        return $this;
    }

    /**
     * 查多条
     */
    public function select(){
        $data = $this->driver->where($this->where)->select();
        $this->data = $data;
        return $this;
    }

    /**
     * 删除
     */
    public function delete(){
        $this->driver->where($this->where)->delete();
    }

    /**
     * 获取最近一直的sql
     * @return string
     */
    public function getLastSql(){
        return $this->driver->getLastSql();
    }

    /**
     * 过滤掉数组中的null值
     * @param array $row
     * @return array
     */
    private function _filterNull(array $row){
        //过滤掉对象中的null
        return array_filter($row, function ($item){
            return !is_null($item); //当内置函数返回false时，过滤
        });
    }
    //----------------------------------------------------ArrayAccess---------------------------------------------------
    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset){
        return isset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset){
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value){
        $this->data[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset){
        unset($this->data[$offset]);
    }


}