<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 18:35
 *
 * tip - `任务表`实体
 */
namespace Iflycc\Notify\dao\entity;
use Iflycc\Notify\service\Func;

class RetryTasksEntity implements IEntity {
    public $id; //主键id | `id`
    public $sessionId; //会话sessionId | `session_id`
    public $appId; //游戏id | `app_id`
    public $channelId; //渠道id | `channel_id`
    public $uid; //玩家id | `uid`
    public $versionCode; //版本号 | `version_code`
    public $url; //待发送的url链接 | `url`
    public $httpRequestMethod; //http请求方式 | `http_request_method`
    public $httpRequestParam; //http 请求参数 | `http_request_param`
    public $isDes; //是否Des加密 | `is_des`
    public $retryTimes; //重发的次数 | `try_times`
    public $doneTimes; //已执行的次数 | `done_times`
    public $lastSendTime; //最后一次发送的时间 | `last_send_time`
    public $createdAt; //创建于 | `created_at`

    /**
     * 对象转数组
     * @return array
     */
    public function toArray(){
        $objArr = json_decode(json_encode($this), true) ?: [];
        $result = [];
        foreach ($objArr as $_key => $_val){
            $_field = Func::convertUcWordTo_($_key);
            $result[$_field] = $_val;
        }
        return $result;
    }
}