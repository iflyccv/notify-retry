<?php
/**
 * Author: iflycc
 * Time: 2023.07.28 11:21
 *
 * tip - 实体接口
 */
namespace Iflycc\Notify\dao\entity;

interface IEntity{
    /**
     * 对象转为数组
     * @return mixed
     */
    function toArray();
}