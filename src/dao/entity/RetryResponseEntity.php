<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 18:36
 *
 * tip - `重发响应表`实体
 */
namespace Iflycc\Notify\dao\entity;

use Iflycc\Notify\service\Func;

class RetryResponseEntity implements IEntity {
    public $id; //主键id | `id`
    public $sessionId; //session会话id | `session_id`
    public $appId; //游戏id | `app_id`
    public $channelId; //渠道id | `channel_id`
    public $uid; //玩家id | `uid`
    public $versionCode; //版本号 | `version_code`
    public $url; //请求链接 | `url`
    public $httpRequestMethod; //http请求方式 | `http_request_method`
    public $isSuccess; //是否通知成功 | `is_success`
    public $httpRequestParam; //http请求参数 | `http_request_param`
    public $isDes; //是否Des加密 | `is_des`
    public $httpResponseOrigin; //http对端响应（原生返回） | `http_response_origin`
    public $httpResponseAfterDes; //http对端响应（Des解密后） | `http_response_after_des`
    public $remark; //备注 | `remark`
    public $createdAt; //创建于 | `created_at`

    /**
     * 对象转数组
     * @return array
     */
    public function toArray(){
        $objArr = json_decode(json_encode($this), true) ?: [];
        $result = [];
        foreach ($objArr as $_key => $_val){
            $_field = Func::convertUcWordTo_($_key);
            $result[$_field] = $_val;
        }
        return $result;
    }
}