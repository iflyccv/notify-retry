<?php
/**
 * Author: iflycc
 * Time: 2023.07.29 23:26
 *
 * tip - 模型父类
 */
namespace Iflycc\Notify\dao;

use Iflycc\Notify\service\Func;

abstract class Model{

    protected $driver;

    /**
     * 抽象方法，根据不同的数据库实现驱动，依情况加载
     * @return mixed
     */
    abstract public function db();


    /**
     * 单例、禁止外部初始化控制
     * RetryResponseModel constructor.
     */
    protected function __construct(){
        $this->driver = Func::config('db.driver'); //数据库驱动
    }

    /**
     * 静态魔术调用
     * @param string $method 方法名
     * @param mixed $arguments 参数
     * @return false|mixed
     */
    public static function __callStatic($method, $arguments){
        $model = new static();
        return call_user_func_array( [ $model->db(), $method], $arguments );
    }

    /**
     * 魔术调用
     * @param string $method 方法名
     * @param mixed $arguments 参数
     * @return false|mixed
     */
    public function __call($method, $arguments){
        return call_user_func_array( [ $this->db(), $method], $arguments );
    }
}