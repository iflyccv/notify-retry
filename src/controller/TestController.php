<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 15:37
 *
 * tip - 通知重发
 */
namespace Iflycc\Notify\controller;


use Iflycc\Notify\dao\entity\RetryResponseEntity;
use Iflycc\Notify\dao\RetryResponseModel;
use Iflycc\Notify\dao\RetryTasksModel;
use Iflycc\Notify\service\Params;
use Iflycc\Notify\service\Des;
use Iflycc\Notify\service\entities\NotifyConfigEntity;
use Iflycc\Notify\service\Func;
use Iflycc\Notify\service\Tasks;

class TestController{

    public function test(){
        echo '<pre />';
        echo 'Iflycc\Notify\controller\TestController.test() running......',PHP_EOL;


        echo '<hr />';
        $notifyConf = Func::config("notifications.100.c1");
        $notifyEntity = NotifyConfigEntity::toEntity($notifyConf);
        print_r($notifyEntity);

        echo '<hr />';
//——————————————————————————————————————————————————————Mysql Test——————————————————————————————————————————————————————
        $taskModel = RetryTasksModel::where('')->find();
        print_r($taskModel->toEntity());
        $retryResponseModel = RetryResponseModel::where('')->find();
        print_r($retryResponseModel->toEntity());
        print_r($retryResponseModel->toEntity()->toArray());
        exit;



//——————————————————————————————————————————————————————Mongo Test——————————————————————————————————————————————————————
//        $taskModel = RetryTasksModel::where('')->find();
//        print_r($taskModel->toEntity());
//        $retryResponseModel = RetryResponseModel::where(['id' => '64d1e150e11c00008e002cc3'])->find();
//        print_r($retryResponseModel->toEntity());
//        print_r($retryResponseModel->toEntity()->toArray());
//        $responseEntity = new RetryResponseEntity();
//        $responseEntity->appId = 200;
//        $responseEntity->channelId = "8000001";
//        $responseEntity->uid = "574261554";
//        $responseEntity->versionCode = 33;
//        $responseEntity->url = "http://localhost:8444/n_nor";
//        $responseEntity->httpRequestMethod = "GET";
//        $responseEntity->isSuccess = 0;
//        $responseEntity->httpRequestParam = '{"name":"cc","age":20}';
//        $responseEntity->isDes = 0;
//        $responseEntity->httpResponseOrigin = "";
//        $responseEntity->httpResponseAfterDes = "";
//        $responseEntity->remark = "remark";
//        $responseEntity->createdAt = date('Y-m-d H:i:s');
//        RetryResponseModel::where(['session_id' => 'ses6bb8322c716004335f7a778bf2fff'])->update($responseEntity);
//        exit;


        //生成一个重试任务
        $notifyConf = Func::config("notifications.100.c2");
        print_r($notifyConf);

        $notifyEntity = NotifyConfigEntity::toEntity($notifyConf);
        $httpRequestParam = [
            'session_id' => 'ses89351b27035d9f175b0a9a913fedf',
            'hash_code' => '5c08525c33f0a7729068fbff2c26999d',
            'app_id' => 160194,
            'guid' => 223022220000001,
            'value' => '{"status":1,"data":{"df15":1}}',
            'title' => 'rechargeGoodsMail_2023/08/02',
            'content' => '2023-08-02+10:44:37+Dear+player,+the+goods+you+bought+have+been+sent+to+your+mail,+please+check',
            'type' => 12,
            'product_id' => 'df15'
        ];
        Tasks::notifyingGame(100,'8000000',31,'574261555',$httpRequestParam, $notifyEntity);

    }


}