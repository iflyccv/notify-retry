<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 16:19
 *
 * tip - Des加密/解密
 */
namespace Iflycc\Notify\service;

class Des{
    const SECRET_KEY = 'abc12345';

#region     1. 加密
    /**
     * @param $input
     * @param string $key
     * @return string
     */
    public static function encrypt($input, $key = self::SECRET_KEY) {
        if(PHP_VERSION < 7){ //php 5的版本
            $size = mcrypt_get_block_size('des', 'ecb');
            $input = self::pkcs5_pad($input, $size);
            $td = mcrypt_module_open('des', '', 'ecb', '');
            $iv = @mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
            @mcrypt_generic_init($td, $key, $iv);
            $data = mcrypt_generic($td, $input);
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            $data = base64_encode($data);
        }else{ //php 7的版本
            $des = @openssl_encrypt($input, 'des-ecb', $key, OPENSSL_RAW_DATA, '');
            $data = base64_encode($des);
        }

        return trim($data);
    }

    /**
     * @param $text
     * @param $blockSize
     * @return string
     */
    private static function pkcs5_pad ($text, $blockSize) {
        $pad = $blockSize - (strlen($text) % $blockSize);
        return $text . str_repeat(chr($pad), $pad);
    }
#endregion


#region     2. 解密
    /**
     * @param $encrypted
     * @param string $key
     * @return bool|string
     */
    public static function decrypt($encrypted, $key = self::SECRET_KEY) {
        if(PHP_VERSION < 7){ //php 5的版本
            $encrypted = base64_decode($encrypted);
            $td = mcrypt_module_open('des','','ecb','');
            //使用MCRYPT_DES算法,cbc模式
            $iv = @mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
            mcrypt_enc_get_key_size($td);
            @mcrypt_generic_init($td, $key, $iv);
            //初始处理
            $decrypted = mdecrypt_generic($td, $encrypted);
            //解密
            mcrypt_generic_deinit($td);
            //结束
            mcrypt_module_close($td);
            $result = self::pkcs5_unpad($decrypted);
        }else{ //php 7的版本
            $result = @openssl_decrypt(base64_decode($encrypted), 'des-ecb', $key, OPENSSL_RAW_DATA, '');
        }
        return $result;
    }

    /**
     * @param $text
     * @return bool|string
     */
    private static function pkcs5_unpad($text) {
        $pad = @ord($text[strlen($text)-1]);
        if ($pad > strlen($text))
            return false;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad)
            return false;
        return substr($text, 0, -1 * $pad);
    }
#endregion
}