<?php
/**
 * Author: iflycc
 * Time: 2023.08.02 15:23
 *
 * tip - “通知配置”实体类
 */
namespace Iflycc\Notify\service\entities;
use Iflycc\Notify\service\Func;

class NotifyConfigEntity extends AbstractServiceEntity {
    public $url;                //通知地址
    public $httpRequestMethod;  //http请求方法
    public $retryTimes;         //重发的次数
    public $isDes;              //是否是Des加密

    /**
     * 对象转数组
     * @return array
     */
    public function toArray(){
        $objArr = json_decode(json_encode($this), true) ?: [];
        $result = [];
        foreach ($objArr as $_key => $_val){
            $_field = Func::convertUcWordTo_($_key);
            $result[$_field] = $_val;
        }
        return $result;
    }
}