<?php
/**
 * Author: iflycc
 * Time: 2023.08.02 15:44
 *
 * tip - service的实体类抽象父类
 */

namespace Iflycc\Notify\service\entities;


use Exception;
use Iflycc\Notify\service\Func;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractServiceEntity{
    private $entityVarsConvertFields = []; //实体类的自定义的字段

    /**
     * AbstractServiceEntity constructor.
     */
    public function __construct(){
        //根据`实体类`初始化数据库表字段
        $ref = new ReflectionClass(static::class); //获取static对象的反射句柄
        foreach ($ref->getProperties() as $_reflectPropertyObj){
            //仅转换static类自身的属性，排除父类属性
            if($_reflectPropertyObj instanceof ReflectionProperty && $_reflectPropertyObj->class == static::class){
                $this->entityVarsConvertFields[] = Func::convertUcWordTo_($_reflectPropertyObj->name);
            }
        }
    }

    /**
     * array转为entity对象实例
     * @param array $data
     * @return static|null
     */
    public static function toEntity(array $data){
        $result = null;
        try {
            if(!$data) return null;
            $entityObj = new static(); //实例化实体类
            if(array_diff($entityObj->entityVarsConvertFields, array_keys($data))){
                throw new Exception('非法的数据，data数据转换为对象失败，请确认数据源与实体对象的字段对应关系！');
            }
            //通过检测，则退出循环
            foreach ($entityObj->entityVarsConvertFields as $_field){
                $_var = Func::convert_ToUcWord($_field);
                $entityObj->$_var = $data[$_field];
            }
            $result = $entityObj;
        }catch (Exception $e){
            $err = [
                'err_file' => $e->getFile(),
                'err_line' => $e->getLine(),
                'err_previous' => $e->getPrevious(),
                'err_message' => $e->getMessage(),
                'err_track' => $e->getTraceAsString(),
            ];
            echo json_encode($err, JSON_UNESCAPED_UNICODE); exit;
        }
        return $result;
    }
}