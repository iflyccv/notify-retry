<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 17:38
 *
 * tip - 重发任务service
 */
namespace Iflycc\Notify\service;
use Iflycc\Notify\dao\entity\RetryResponseEntity;
use Iflycc\Notify\dao\entity\RetryTasksEntity;
use Iflycc\Notify\dao\RetryResponseModel;
use Iflycc\Notify\dao\RetryTasksModel;
use Iflycc\Notify\service\entities\NotifyConfigEntity;

class Tasks{

#region     1. 通知游戏服务器
    /**
     * @param $appId
     * @param $channelId
     * @param $versionCode
     * @param $uid
     * @param array $httpRequestParam 通知对方的请求参数，如：{"value":"1_1_100", "name":"张三"}
     * @param NotifyConfigEntity $notifyEntity 通知的对象，如果存在，则
     * @return bool
     */
    public static function notifyingGame($appId, $channelId, $versionCode, $uid, array $httpRequestParam, NotifyConfigEntity $notifyEntity){
        //① 生成sessionId
        $sessionId = Tasks::_sessionIdGenerate();
        //根据 $contents 的类型判断，组装
        $httpRequestParam['session_id'] = $sessionId;
        //② 执行重发
        $isNotifySuccess = Tasks::sending($appId, $channelId, $versionCode, $uid, $sessionId, $notifyEntity->url,
            $notifyEntity->httpRequestMethod, $notifyEntity->isDes, $httpRequestParam);
        if(!$isNotifySuccess){ //如果重发失败了，则保存到`重发任务表`中，执行定时发送
            $retryTasksEntity = new RetryTasksEntity();
            $retryTasksEntity->sessionId = $sessionId;
            $retryTasksEntity->appId = $appId;
            $retryTasksEntity->channelId = $channelId;
            $retryTasksEntity->uid = $uid;
            $retryTasksEntity->versionCode = $versionCode;
            $retryTasksEntity->url = $notifyEntity->url;
            $retryTasksEntity->httpRequestMethod = $notifyEntity->httpRequestMethod;
            $retryTasksEntity->httpRequestParam = urlencode(json_encode($httpRequestParam, JSON_UNESCAPED_UNICODE));
            $retryTasksEntity->retryTimes = $notifyEntity->retryTimes;
            $retryTasksEntity->doneTimes = 0;
            $retryTasksEntity->isDes = $notifyEntity->isDes;
            $retryTasksEntity->lastSendTime = '';
            $retryTasksEntity->createdAt = date('Y-m-d H:i:s');
            RetryTasksModel::add($retryTasksEntity); //入库
        }
        //③ 返回结果
        return $isNotifySuccess;
    }

    /**
     * 生成会话sessionId
     * @return string
     */
    private static function _sessionIdGenerate(){
        return 'ses' . substr(sha1(microtime(true)), 0,29); //32位字符串
    }
#endregion


#region     2. 发送远程任务通知
    /**
     * @param int $appId
     * @param string $channelId
     * @param int $versionCode
     * @param string $uid
     * @param string $sessionId
     * @param string $url
     * @param string $httpRequestMethod
     * @param int $isDes 是否Des加密，如果=1，则对响应response则Des解密操作
     * @param array $httpRequestParam
     * @return bool
     */
    public static function sending($appId, $channelId, $versionCode, $uid, $sessionId, $url, $httpRequestMethod, $isDes, array $httpRequestParam){
        $isSuccess = false; //是否任务执行成功
        //① 发送请求
        $responseOrigin = null; //返回的响应的原始数据
        $httpParamReq = http_build_query($httpRequestParam); //将数组转为http请求参数格式：k1=v1&k2=v2
        switch ($httpRequestMethod){
            case 'GET':
                $_url = "{$url}?{$httpParamReq}"; //拼接完整的GET请求链接
                $responseOrigin = Func::curl($_url);
                break;
            case 'POST':
                $responseOrigin = Func::curl($url, $httpParamReq);
                break;
            default:
                return $isSuccess; //非 GET/POST，则不执行本次任务
        }
        //② 解析响应结果（对端服务器的响应）
        $response = ''; //解析后的响应结果
        if($responseOrigin){ //响应信息不为空，则进行解析
            if($isDes){ //如果Des解密，则返回值也是Des加密，则需要对响应结果进行Des解密处理
                $responseDesString = str_replace(' ', '+', $responseOrigin); //空格替换为+号
                $responseJson = Des::decrypt($responseDesString);
            }else{
                $responseJson = $responseOrigin; //非加密模式下，则认为返回值就是一个 json 串
            }
            $response = json_decode($responseJson,true); //解析成数组
            if(is_array($response) && isset($response['status']) && $response['status'] == 1){ //response.status = 1，唯一成功标识
                $isSuccess = true; //通知成功
            }
        }
        //③ 记录本次通知结果到`通知响应表`
        $retryResponseEntity = new RetryResponseEntity();
        $retryResponseEntity->sessionId = $sessionId;
        $retryResponseEntity->appId = $appId;
        $retryResponseEntity->channelId = $channelId;
        $retryResponseEntity->uid = $uid;
        $retryResponseEntity->versionCode = $versionCode;
        $retryResponseEntity->url = $url;
        $retryResponseEntity->isSuccess = $isSuccess ? 1 : 0; //是否通知成功;
        $retryResponseEntity->httpRequestMethod = $httpRequestMethod;
        $retryResponseEntity->httpRequestParam = urlencode(json_encode($httpRequestParam ,JSON_UNESCAPED_UNICODE));
        $retryResponseEntity->isDes = $isDes; //是否Des加解密
        $retryResponseEntity->httpResponseOrigin = $responseOrigin; //对端服务的原生响应数据
        if($isDes){ //如果是Des加密模式，则需要保存解密后的响应结果
            $retryResponseEntity->httpResponseAfterDes = is_array($response) ? json_encode($response, JSON_UNESCAPED_UNICODE) : ''; //只有正确的解析的json才会被记录到该字段
        }else{
            $retryResponseEntity->httpResponseAfterDes = '';
        }
        $retryResponseEntity->remark = '';
        $retryResponseEntity->createdAt = date('Y-m-d H:i:s'); //记录响应的最原始数据
        RetryResponseModel::add($retryResponseEntity); //入库
        //④ 返回结果
        return $isSuccess;
    }
#endregion
}