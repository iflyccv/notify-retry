<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 16:26
 *
 * tip - 错误提示
 */
namespace Iflycc\Notify\service;

final class ErrorTip{
#region     1. 错误码
    const FAIL = 0 ;//通用失败
    const OK = 1 ;//成功的唯一标识码

    //请求参数检测
    const UID_INVALID = 10001 ;//非法`guid`
    const PARAM_MUST_ERR  = 10002; //必须参数检测错误，参数不存在 or 为空
    const PARAM_ISSET_ERR = 10003; //必须存在参数检测错误，参数不存在
    const PARAM_JSON_ERR = 10004; //参数json格式错误

    //支付
    const PAY_PLAT_ERR = 20001; //错误的支付平台
    const PAY_CREATE_ORDER_ERR = 20002; //下单失败

    //账号中心 【account center】
    const ACCOUNT_TOKEN_EXPIRED = 30001; //token已失效，请重新登录

    //兑换码
    const CdKey_EMPTY = 40001; //cdKey为空或过期
    const CdKey_SERVER_ERR = 40002; //兑换码区服验证失败
    const CdKey_CHANNEL_ERR = 40003; //兑换码渠道验证失败
    const CdKey_SAME_BATCH_USED = 40004; //已使用过当前批次的兑换码
    const CdKey_REJECT_BATCH_USED = 40005; //已使用过互斥批次的兑换码
#endregion


#region    2. 错误提示信息
    public static $msg = [
        ErrorTip::FAIL => 'fail', //失败
        ErrorTip::OK => 'ok', //成功的唯一标识码：1

        //请求参数错误码
        ErrorTip::UID_INVALID => '`uid` err' ,
        ErrorTip::PARAM_MUST_ERR    => '[params.must err]参数错误' ,
        ErrorTip::PARAM_ISSET_ERR   => '[params.isset err]参数检测失败' ,
        ErrorTip::PARAM_JSON_ERR => '参数json格式错误' ,

        //支付
        ErrorTip::PAY_PLAT_ERR => '非法的支付平台', //错误的支付平台
        ErrorTip::PAY_CREATE_ORDER_ERR => '下单失败',

        //账号中心 【account center】
        ErrorTip::ACCOUNT_TOKEN_EXPIRED => 'token已失效，请重新登录',

        //兑换码
        ErrorTip::CdKey_EMPTY => '兑换码不存在或已过期',
        ErrorTip::CdKey_SERVER_ERR => '兑换码serverId验证失败',
        ErrorTip::CdKey_CHANNEL_ERR => '兑换码channelId验证失败',
        ErrorTip::CdKey_SAME_BATCH_USED => '已使用过当前批次的兑换码',
        ErrorTip::CdKey_REJECT_BATCH_USED => '已使用过互斥批次的兑换码',
    ];
#endregion
}