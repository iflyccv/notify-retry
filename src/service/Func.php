<?php
/**
 * Author: iflycc
 * Time: 2023-7-29 17:12:11
 *
 * tip - 公共函数类
 */
namespace Iflycc\Notify\service;

class Func{

#region     1. 获取ip地址
    /**
     * @return string
     */
    public static function ip(){
        if (isset($_SERVER['HTTP_X_REAL_IP'])) {//nginx 代理模式下，获取客户端真实IP
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {//客户端的ip
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {//浏览当前页面的用户计算机的网关
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) unset($arr[$pos]);
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];//浏览当前页面的用户计算机的ip地址
        } else {
            $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        }
        return $ip;
    }
#endregion


#region     2. 响应错误的日志
    /**
     *
     * @param string $dirName 文件名称
     * @param array $req
     * @param array $resp
     * @param string $logPath 日志目录
     */
    public static function response_error_log($dirName, array $req, array $resp, $logPath = '/data/log/game'){
        // 日志记录新格式
        $registerLog = [
            'uri'   => isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '', //请求的路由
            'req'   => [
                'client' => $req,
                'post' => $_POST,
                'get' => $_GET,
            ],
            'resp'  => $resp,
            'ip'    => self::ip(),
            "time"  => date("Y-m-d H:i:s")
        ];
        Log::pushJson($dirName, 'C-notify', $registerLog, $logPath);
    }
#endregion


#region     3. 发生curl请求
    /**
     * @param string $url
     * @param mixed $postData
     * @param array $header 请求头
     * @return bool|string
     */
    public static function curl($url, $postData = null, $header = []){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER,false);
        if(!empty($header)){
            curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
        }
        if(strpos($url,'https') !== false){
            curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        }
        if(!empty($postData)){
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$postData);
        }
        $result = curl_exec($ch);
        // 检查是否有错误发生
        if(curl_errno($ch)) {
            return 'Curl error: ' . curl_error($ch);
        }
        return $result;
    }
#endregion


#region     4. 读取配置
    /**
     * 获取和设置配置参数，查询方式：
     *      - ① `db.`    ： 查找 config/db.php中的全部数据
     *      - ② `db.ip`  ： 查找 config/db.php中的数组key=ip的数值
     *      - ③ `db.else.key`    ： 查找 config/db.php中的数组 $data.else.key 的值
     *
     * @param string|array  $name 参数名（只支持二级查询），如： name.key  / name.key1.key2
     * @return mixed
     */
    public static function config($name = ''){
        $explodes = explode('.', $name);
        $result = '';
        switch (count($explodes)){
            case 2:
                $k1 = $explodes[0];
                $k2 = $explodes[1];
                if(file_exists(__DIR__ . "/../config/{$k1}.php")){
                    $data = include __DIR__ . "/../config/{$k1}.php";
                    if(!$k2){
                        $result = $data;
                    }elseif(is_array($data) && isset($data[$k2])){
                        $result = $data[$k2];
                    }
                }
                break;
            case 3:
                $k1 = $explodes[0];
                $k2 = $explodes[1];
                $k3 = $explodes[2];
                if(file_exists(__DIR__ . "/../config/{$k1}.php")){
                    $data = include __DIR__ . "/../config/{$k1}.php";
                    if(!$k3 && is_array($data[$k2])){
                        $result = $data[$k2];
                    }elseif(is_array($data[$k2]) && isset($data[$k2][$k3])){
                        $result = $data[$k2][$k3];
                    }
                }
                break;
        }
        return $result;
    }
#endregion


#region     5. 驼峰与下划线模式相互转换
    /**
     * 驼峰转下划线
     * 将字符串转换为：下划线形式。如：HelloWorld => hello_world
     * @param $string
     * @return string
     */
    public static function convertUcWordTo_($string){
        $pattern = '/([a-z])([A-Z])/';
        $replacement = '${1}_${2}';
        return strtolower(preg_replace($pattern, $replacement, $string));

    }

    /**
     * 下划线转驼峰
     * 将字符串转换为：下划线形式。如：hello_world => helloWorld（首字符除外）
     * @param $string
     * @return string
     */
    public static function convert_ToUcWord($string){
        $explodes = explode('_', $string);
        $container = [];
        foreach ($explodes as $_index => $_item){
            $_item = strtolower($_item);
            $container[] = ($_index == 0) ? $_item : ucfirst($_item); //第一个元素小写
        }
        return join('', $container);

    }
#endregion
}