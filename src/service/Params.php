<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 16:02
 *
 * tip - 参数接收Service
 */
namespace Iflycc\Notify\service;

class Params {
    // 基础参数
    public $uid;
    public $appId;
    public $channelId;
    public $versionCode;
    public $androidId;
    public $_reqTime_; //客户端校验
    public $allParams; //全部参数
    public $ip; // 请求的IP地址
    public $clientParams; //客户端的完整请求参数
    // DI的注入对象
    public $request; // 请求类
    private static $instance;
    private $isDes = true; //标识是否加密，默认：false

    /**
     * Params constructor.
     */
    public function __construct(){
        $this->request = $_REQUEST;
        $this->_getParams(); // 执行接收参数
    }

    /**
     * @return Params
     */
    public static function instance(){
        if(!(self::$instance instanceof self)){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 接收参数并复制给类的对应属性
     */
    private function _getParams(){
        $requests = $this->_decrypt(); // 获取POST数据并DES解码
        $this->clientParams = $requests;
        $this->uid = array_key_exists('guid', $requests) ? $requests['guid'] : '';
        $this->allParams['uid']         = $this->uid = $this->uid ?: (array_key_exists('uid', $requests) ? $requests['uid'] : '');
        $this->allParams['app_id']      = $this->appId        = array_key_exists('app_id', $requests) ? $requests['app_id'] : '';
        $this->allParams['channel_id']  = $this->channelId    = array_key_exists('channel_id', $requests) ? $requests['channel_id'] : '';
        $this->allParams['versionCode'] = $this->versionCode  = array_key_exists('versionCode', $requests) ? $requests['versionCode'] : '';
        $this->allParams['androidid']   = $this->androidId    = array_key_exists('androidid', $requests) ? $requests['androidid'] : '';
        $this->allParams['ip'] = $this->ip  = Func::ip();
        // 添加附加key
        $this->allParams = array_merge($this->allParams, $requests);
    }

    /**
     * Des 客户端数据解密
     * @return array
     */
    private function _decrypt(){
        if(!$this->isDes)  {
            $data = $this->request;
        } else{
            $data = null;
            if(isset($this->request['data'])){
                $clientData = str_replace(' ', '+', $this->request['data']); //空格替换为+号
                $decryptData = Des::decrypt($clientData);
                $data = $clientData ? json_decode($decryptData, true) : [];
            }
        }
        return $data ?: [];
    }

    /**
     * 切换是否Des加密
     * @param bool $isDes 是否加密
     * @return Params
     */
    public function desChange($isDes){
        if($isDes != $this->isDes){ //设置方式不一样，则重新获取参数
            //重新获取参数
            $this->isDes = $isDes;
            $this->_getParams();
        }
        return $this;
    }


    /**
     * 验证请求的参数的正确性
     * @param array $must 必须存在且不能为空
     * @param array $mustIsset 可以为空，但是不能不存在
     * @return int
     */
    public function validate(array $must = [], array $mustIsset = []){
        //① 检测must
        foreach ($must as $_key){
            if(array_key_exists($_key, $this->allParams) && $this->allParams[$_key]) continue; // 存在且不为空，表示合法
            return ErrorTip::PARAM_MUST_ERR;
        }
        //② 检测mustIsset
        foreach ($mustIsset as $_key){
            if(array_key_exists($_key, $this->allParams)) continue; // 存在，表示合法
            return ErrorTip::PARAM_ISSET_ERR;
        }
        return ErrorTip::OK;
    }

    /**
     * 格式化响应
     *
     * @param int $status 状态码
     * @param array $dataResp 响应的数据
     * @param string $msg 响应提示信息
     */
    public function notify( $status, array $dataResp = [], $msg = ''){
        //① 初始化基本下发数据格式
        $respArr = [
            'status' => $status,
            'msg'    => $msg ?: (array_key_exists($status, ErrorTip::$msg) ? ErrorTip::$msg[$status] : ''),
            'data'   => $dataResp,
            "_reqTime_" => $this->_reqTime_ ?: '',
        ];
        //② 记录error log
        if($status != ErrorTip::OK){
            Func::response_error_log('response_error_log', $this->allParams, $respArr);
        }
        //③ 根据条件判断是否需要加密
        $result = json_encode($respArr,JSON_UNESCAPED_UNICODE);
        if($this->isDes) { //Des加密
            $result = Des::encrypt($result);
        }
        //④ 设置响应头
        $header['Access-Control-Allow-Origin']  = '*';
        $header['Access-Control-Allow-Headers'] = 'X-Requested-With,Content-Type,XX-Device-Type,XX-Token';
        $header['Access-Control-Allow-Methods'] = 'GET,POST,PATCH,PUT,DELETE,OPTIONS';
        $header['Content-Type'] = 'application/json';
        $header['Charset'] = 'utf8';
        //⑤ 发送头部信息
        foreach ($header as $name => $val) {
            header($name . (!is_null($val) ? ':' . $val : ''));
        }
        echo $result;
        exit;
    }

    /**
     * 获取不一定存在传参数的值
     * @param $key
     * @param string $default
     * @return mixed|string
     */
    public function val($key, $default = ''){
        return isset($this->allParams[$key]) ? $this->allParams[$key] : $default;
    }
}