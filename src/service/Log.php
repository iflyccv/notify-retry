<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 16:33
 *
 * tip - 日志处理
 */
namespace Iflycc\Notify\service;

class Log{
    private $fileCh; //文件句柄

#region     1. 写日志
    /**
     * 写日志
     *
     * @param $appId
     * @param string $filenamePrefix 文件名前缀
     * @param array $log 文件内容
     * @param string $path 文件路径
     */
    public static function pushJson($appId, $filenamePrefix, array $log, $path = ''){
        $log = is_array($log) ? json_encode($log, JSON_UNESCAPED_UNICODE) : $log;
        $dir = $path ? "{$path}/{$appId}" : "/data/log/gamelog/{$appId}" ;
        $dt = date('Y-m-d');
        $filename =  "{$filenamePrefix}.{$dt}.log";
        $self = new self();
        $self->_fileOpen($dir, $filename);

        if(!fwrite($self->fileCh, $log . "\n")) die("写入日志失败");//写日志失败
    }
#endregion


    /**
     * 根据 目录、文件名 打开目标文件
     * @param null $dir
     * @param null $filename
     */
    private function _fileOpen($dir = null,$filename = null){
        //默认路径为当前路径
        $filePath = $dir ?: '';
        //默认为以时间＋.log的文件文件
        $fileName = $filename ?: date('Y-m-d') . '.log';
        //生成路径字串
        $path = $this->_createPath($filePath, $fileName);
        if(!file_exists($path)){
            if($filePath && !$this->_createDir($filePath)) die('创建目录失败!');
            if(!$this->_createLogFile($path)) die("创建文件失败!");//创建文件不成功的处理
        }
        $path = $this->_createPath($filePath, $fileName);  //生成路径字串
        $this->fileCh = fopen($path,"a+");//打开文件
    }

    /**
     * 不给new
     * Log constructor.
     */
    private function __construct(){
    }

    /**
     * 创建目录
     * @param $dir
     * @return bool
     */
    private function _createDir($dir){
        return is_dir($dir) or ($this->_createDir(dirname($dir)) and mkdir($dir));
    }

    /**
     * 创建日志文件
     * @param $path
     * @return bool
     */
    private function _createLogFile($path){
        $handle = fopen($path,"w"); //创建文件
        fclose($handle);
        return file_exists($path);
    }

    /**
     * 创建路径
     * @param $dir
     * @param $filename
     * @return string
     */
    private function _createPath($dir,$filename){
        return empty($dir) ? $filename : "{$dir}/{$filename}";
    }

    /**
     * 析构方法
     */
    public  function __destruct(){
        is_resource($this->fileCh) && fclose($this->fileCh);//关闭文件
    }
}