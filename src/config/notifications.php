<?php
/**
 * Author: iflycc
 * Time: 2023.07.28 14:07
 *
 * tip - 通知远程服务器的配置
 */
return [
    // TODO 根据自身情况配置
    '100' => [ //app_id
        //配置1
        'c1' => [
            'url' => 'http://localhost:8444/n_nor' ,
            'http_request_method' => 'POST', //get | post
            'retry_times' => 3, //重试的次数
            'is_des' => 0, //是否是Des加密， | 0：不加密  1：加密
        ],

        //配置2
        'c2' => [
            'url' => 'http://localhost:8444/n_des' ,
            'http_request_method' => 'POST',
            'retry_times' => 3,
            'is_des' => 1,
        ],
    ]

];