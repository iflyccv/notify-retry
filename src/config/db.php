<?php
/**
 * Author: iflycc
 * Time: 2023.07.26 18:55
 *
 * tip - mysql配置
 */

return [
//    'driver'    => 'mongo', //数据库驱动[ mysql | mongo ]
    'driver'    => 'mysql', //数据库驱动[ mysql | mongo ]

    'host'      => '127.0.0.1', //ip地址
    'port'      => '3306',      //端口
//    'port'      => '27017',      //端口
    'database'  => 'retry_notify',      //数据库名
    'username'  => 'root' ,     //用户名
    'password'  => 'root',      //密码
    'else' => [
        'key' => 'key-val'
    ]
];